ARG BASE_IMAGE="alpine:3.15"
FROM $BASE_IMAGE

RUN apk add --no-cache dbus-x11 openjdk11-jre py-cairo py-netifaces py-pip \
        py3-xdg terminus-font ttf-dejavu xhost xpra xpra-webclient && \
    rm -f /var/cache/apk/* && \
    pip install pyinotify && \
    adduser --disabled-password --gecos "jive_user" --uid 1000 user && \
    mkdir -p /run/user/1000/xpra && \
    mkdir -p /run/xpra && \
    chown user:user /run/user/1000/xpra && \
    chown user:user /run/xpra

ADD java /usr/local/java
ADD jive /usr/local/bin
ADD jive.desktop /usr/share/applications
ADD applications.menu /etc/xdg/menus/kde-applications.menu

ARG BUILD_DATE
ARG GIT_REV
ARG DOCKER_TAG="0.1"

LABEL \
    org.opencontainers.image.title="alpine-jive" \
    org.opencontainers.image.description="Jive on Alpine Linux" \
    org.opencontainers.image.authors="GP Orcullo<kinsamanka@gmail.com>" \
    org.opencontainers.image.version=$DOCKER_TAG \
    org.opencontainers.image.url="https://hub.docker.com/repository/docker/kinsamanka/alpine-jive" \
    org.opencontainers.image.source="https://gitlab.com/kinsa13/tango/Docker/alpine-jive" \
    org.opencontainers.image.revision=$GIT_REV \
    org.opencontainers.image.created=$BUILD_DATE

WORKDIR /home/user
USER user
EXPOSE 8081
VOLUME /home/user

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV TANGO_HOST=localhost:10000

CMD rm -rf /tmp/.X* &&  xpra start --start=/usr/local/bin/jive --bind-tcp=0.0.0.0:8081 --daemon=no --xvfb="/usr/bin/Xvfb +extension Composite -screen 0 1920x1080x24+32 -nolisten tcp -noreset"  --pulseaudio=no --notifications=no --bell=no --mdns=no --webcam=no --pulseaudio=no :100
