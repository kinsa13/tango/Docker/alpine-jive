# Jive running on Alpine Linux

To build:
```
docker build --build-arg GIT_REV="$(git rev-parse HEAD)" --build-arg BUILD_DATE="$(date)" -t <tag> .
```

To run:
```
docker run -it -p 8081:8081 <tag>
```

To access:
```
xpra attach tcp://<docker host>:8081
```

or point your browser to: `http://<docker host>:8081

